// generated by cdk8s
import { ApiObject } from 'cdk8s';
import { Construct } from 'constructs';

/**
 * 
 *
 * @schema TLSStore
 */
export class TlsStore extends ApiObject {
  /**
   * Defines a "TLSStore" API object
   * @param scope the scope in which to define this object
   * @param name a scope-local name for the object
   * @param options configuration options
   */
  public constructor(scope: Construct, name: string, options: TlsStoreOptions = {}) {
    super(scope, name, {
      ...options,
      kind: 'TLSStore',
      apiVersion: 'traefik.containo.us/v1alpha1',
    });
  }
}

/**
 * @schema TLSStore
 */
export interface TlsStoreOptions {
}

