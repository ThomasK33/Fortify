// generated by cdk8s
import { ApiObject } from 'cdk8s';
import { Construct } from 'constructs';

/**
 * 
 *
 * @schema RedisVersion
 */
export class RedisVersion extends ApiObject {
  /**
   * Defines a "RedisVersion" API object
   * @param scope the scope in which to define this object
   * @param name a scope-local name for the object
   * @param options configuration options
   */
  public constructor(scope: Construct, name: string, options: RedisVersionOptions = {}) {
    super(scope, name, {
      ...options,
      kind: 'RedisVersion',
      apiVersion: 'catalog.kubedb.com/v1alpha1',
    });
  }
}

/**
 * @schema RedisVersion
 */
export interface RedisVersionOptions {
  /**
   * @schema RedisVersion#metadata
   */
  readonly metadata?: any;

  /**
   * RedisVersionSpec is the spec for redis version
   *
   * @schema RedisVersion#spec
   */
  readonly spec?: RedisVersionSpec;

}

/**
 * RedisVersionSpec is the spec for redis version
 *
 * @schema RedisVersionSpec
 */
export interface RedisVersionSpec {
  /**
   * Database Image
   *
   * @schema RedisVersionSpec#db
   */
  readonly db: RedisVersionSpecDb;

  /**
   * Deprecated versions usable but regarded as obsolete and best avoided, typically due to having been superseded.
   *
   * @schema RedisVersionSpec#deprecated
   */
  readonly deprecated?: boolean;

  /**
   * Exporter Image
   *
   * @schema RedisVersionSpec#exporter
   */
  readonly exporter: RedisVersionSpecExporter;

  /**
   * PSP names
   *
   * @schema RedisVersionSpec#podSecurityPolicies
   */
  readonly podSecurityPolicies: RedisVersionSpecPodSecurityPolicies;

  /**
   * Version
   *
   * @schema RedisVersionSpec#version
   */
  readonly version: string;

}

/**
 * Database Image
 *
 * @schema RedisVersionSpecDb
 */
export interface RedisVersionSpecDb {
  /**
   * @schema RedisVersionSpecDb#image
   */
  readonly image: string;

}

/**
 * Exporter Image
 *
 * @schema RedisVersionSpecExporter
 */
export interface RedisVersionSpecExporter {
  /**
   * @schema RedisVersionSpecExporter#image
   */
  readonly image: string;

}

/**
 * PSP names
 *
 * @schema RedisVersionSpecPodSecurityPolicies
 */
export interface RedisVersionSpecPodSecurityPolicies {
  /**
   * @schema RedisVersionSpecPodSecurityPolicies#databasePolicyName
   */
  readonly databasePolicyName: string;

}

